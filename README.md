# Sustainable_Chef
Sustainable chef is a website to track food usage in an effort to curb food waste and suggest new recipes to cook

## Getting started

Clone the repository using the command 
```
Git clone
```
## PreRequisites Needed

> Pipenv, for instructions on how to install go to https://github.com/pypa/pipenv

### Running the server

Once the repository has been cloned navigate inside the project and run the command
```
pipenv shell
```
>This will open up a shell in which all the packages for this project lives

Next run the command 
```
pipenv install
```
>This will install all the necessary packages for the project much like npm or yarn

Whenever you have closed terminal and reopen it to run the server again pipenv shell must always be ran to open the virtual enviornment

Next navigate to sustainable_chef, hint: manage.py should be there, and run the command
```
python manage.py runserver
```
Open a webbrowser and navigate to the url shown in the terminal and you have a live server.




