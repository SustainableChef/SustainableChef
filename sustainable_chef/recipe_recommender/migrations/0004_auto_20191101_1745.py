# Generated by Django 2.2.6 on 2019-11-02 00:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipe_recommender', '0003_auto_20191101_1651'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user_ingredients',
            name='food_id',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='user_recipes',
            name='recipe_id',
            field=models.CharField(max_length=50),
        ),
    ]
