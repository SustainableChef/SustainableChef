from django.http import HttpResponse, JsonResponse, QueryDict
from django.views.decorators.csrf import csrf_exempt
import json
from recipe_recommender.models import User_Ingredients, User_Recipes
from recipe_recommender.serializers import IngredientSerializer, RecipeSerializer
from django.shortcuts import render
import requests
from decouple import config
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from datetime import datetime, timedelta, date
import calendar

@login_required
def scanner(request):
    payload = {'user_id': request.user.id}
    user_pantry = requests.get('http://127.0.0.1:8000/ingredients/', params=payload)
    user_pantry = json.loads(user_pantry.content)
    return render(request, 'recipe_recommender/live_w_locator.html',
    {
        'year_range': range(2019, 3000),
        'day_range': range(1, 32),
        'user_pantry': user_pantry
    })

def recipebyingredients(request):
    #TODO check and see if missing and used ingredients appears in every call, if so, api calls can be reduced by 1
    user_id = request.user.id
    if not user_id:
        user_id = request.GET['user_id']
    all_ingredients = User_Ingredients.objects.filter(user=user_id).order_by('-score').values()
    ten_ingredients = all_ingredients[:10]
    ingredients_string = ''
    for ingredient in ten_ingredients:
        ingredients_string += ingredient['tags'] + ','

    recipe_api_url = config('GET_RECIPE_BY_INGREDIENT')
    querystring = {"number":"5","ranking":"1","ignorePantry":"false","ingredients":ingredients_string[:-1]}
    headers = {
        'x-rapidapi-host': config('SPOON_HOST', cast=str),
        'x-rapidapi-key': config('SPOON_PASSWORD', cast=str)
        }
    response = requests.request("GET", recipe_api_url, headers=headers, params=querystring).json()
    bulk_ids = ''
    for recipe in response:
        bulk_ids += str(recipe['id']) + ','
        recipe['joined_title'] = recipe['title'].replace(' ', '_')

    bulk_url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/informationBulk"
    bulkquerystring = {"ids":bulk_ids[:-1]}
    bulkresponse = requests.request("GET", bulk_url, headers=headers, params=bulkquerystring)
    bulkresponse = json.loads(bulkresponse.content)

    ingredients_dict = {}
    for ingredient in all_ingredients:
        if ingredient['tags'] in ingredients_dict:
            if ingredient['score'] > ingredients_dict[ingredient['tags']]:
                ingredients_dict[ingredient['tags']] = ingredient['score']
        else:
            ingredients_dict[ingredient['tags']] = ingredient['score']

    recipe_score = 0
    used_items = 0
    for recipe in bulkresponse:
        for ingredient in recipe['extendedIngredients']:
            if ingredient['name'] in ingredients_dict:
                used_items += 1
                recipe_score += ingredients_dict[ingredient['name']]
            else:
                recipe_score -= 1
        recipe['sc_score'] = recipe_score
        recipe['used_items'] = used_items
        recipe_score = 0
        used_items = 0
    
    sorted_recipes = sorted(bulkresponse, key = lambda i: i['sc_score'],reverse=True) 

    return JsonResponse(sorted_recipes, safe=False)

def expiring_items(request):
    if request.method == 'GET':
        user_id = None
        try:
            user_id = request.GET['user_id']
        except:
            user_id = request.user.id

        payload_fullsize = {'user_id': user_id}
        all_ingredients = User_Ingredients.objects.filter(user=user_id).values()
        today = datetime.today()
        start_of_week = today - timedelta(days=today.weekday())  # Monday
        end_of_week = start_of_week + timedelta(days=6)  # Sunday
        end_of_month_init = calendar.monthrange(today.year, today.month)
        end_of_month = date(today.year, today.month, end_of_month_init[1])
        expiring_today = 0
        expiring_week = 0
        expiring_month = 0
        for ingredient in all_ingredients:
            #expire_date = datetime.strptime(ingredient['expiration'], "%Y-%m-%dT%H:%M:%SZ")
            expire_date = ingredient["expiration"]
            if expire_date.date() == today.date():
                expiring_today += 1
                expiring_week += 1
                expiring_month += 1
            elif expire_date.date() <= end_of_week.date():
                expiring_week += 1
                expiring_month += 1
            elif expire_date.date() <= end_of_month:
                expiring_month += 1
        return JsonResponse({'Items Expiring Today':expiring_today, 'Items Expiring This Week': expiring_week, 'Items Expiring This Month': expiring_month})

@csrf_exempt
def save_recipes(request):
    if request.method == 'GET':
        user_id = None
        size = None
        try:
            user_id = request.GET['user_id']
        except:
            user_id = request.user.id
        try:
            size = request.GET['size']
            size = int(size)
        except:
            pass
        if user_id:
            if size:
                recipes = User_Recipes.objects.filter(user=user_id).values()[:size]
                return JsonResponse(list(recipes), safe=False)
            else:
                recipes = User_Recipes.objects.filter(user=user_id).values()
                return JsonResponse(list(recipes), safe=False)
        else:
            return JsonResponse({'message':'No user given'})
    elif request.method == 'POST':
        user_id = None
        try:
            user_id = request.GET['user_id']
        except:
            user_id = request.user.id
        data = json.loads(request.body)
        serializer = RecipeSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == 'DELETE':
        delete = QueryDict(request.body)
        recipe_id = delete.get('recipe_id')
        user_id = delete.get('user_id')
        User_Recipes.objects.filter(user=user_id, id=recipe_id).delete()
        return JsonResponse({'Message':'Deleted ' + str(recipe_id)})

@login_required
def home(request):
    payload = {'user_id': request.user.id}
    saved_recipes = User_Recipes.objects.filter(user=request.user.id).values()
    print(saved_recipes)
    expiring_items = requests.get('http://127.0.0.1:8000/expiring/', params=payload)
    expiring_items = json.loads(expiring_items.content)
    user_pantry = requests.get('http://127.0.0.1:8000/ingredients/', params=payload)
    user_pantry = json.loads(user_pantry.content)
    rec_recipes = requests.get('http://127.0.0.1:8000/recipes/', params=payload)
    rec_recipes = json.loads(rec_recipes.content)
    return render(request, 'recipe_recommender/home.html',
    {
        'saved_recipes': saved_recipes,
        'expiring_items': expiring_items,
        'user_pantry' : user_pantry,
        'rec_recipes': rec_recipes
    })


def recipe(request, recipe_id):
    recipe_api_url = config('GET_RECIPE_INGREDIENTS')

    querystring = '/' + str(recipe_id) + '/information'

    recipe_api_url += querystring

    headers = {
        'x-rapidapi-host': config('SPOON_HOST', cast=str),
        'x-rapidapi-key': config('SPOON_PASSWORD', cast=str)
        }

    response = requests.request("GET", recipe_api_url, headers=headers).json()
    return render(request, 'recipe_recommender/recipe.html',
    {
        'name': response['title'],
        'data': response
    })

@csrf_exempt
def ingredient_list(request):
    if request.method == 'GET':
        user_id = None
        size = None
        try:
            user_id = request.GET['user_id']
        except:
            user_id = request.user.id
        try:
            size = request.GET['size']
            size = int(size)
        except:
            pass
        if user_id:
            if size:
                ingredients = User_Ingredients.objects.filter(user=user_id).order_by('-score').values()[:size]
                return JsonResponse(list(ingredients), safe=False)
            else:
                ingredients = User_Ingredients.objects.filter(user=user_id).order_by('-score').values()
                return JsonResponse(list(ingredients), safe=False)
        else:
            return JsonResponse({'message':'No User given'})
    elif request.method == 'POST':
        data = json.loads(request.body)
        serializer = IngredientSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)
    elif request.method == 'DELETE':
        delete = QueryDict(request.body)
        food_id = delete.get('food_id')
        user_id = delete.get('user_id')
        User_Ingredients.objects.filter(user=user_id, id=food_id).delete()
        return JsonResponse({'Message':'Deleted ' + str(food_id)})