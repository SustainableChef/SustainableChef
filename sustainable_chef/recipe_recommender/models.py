from django.db import models
from django.conf import settings

class User_Recipes(models.Model):
    recipe_id = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    image = models.CharField(max_length=250)
    recipe_url = models.CharField(max_length=300, default='')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

class User_Ingredients(models.Model):
    upc_code = models.CharField(max_length=50)
    title = models.CharField(max_length=100)
    expiration = models.DateTimeField()
    score = models.FloatField()
    tags = models.CharField(max_length=50, blank=True)
    qty = models.IntegerField(default=1)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    