from rest_framework import serializers
from recipe_recommender.models import User_Ingredients, User_Recipes
class IngredientSerializer(serializers.ModelSerializer):

    class Meta:
        model=User_Ingredients
        fields = (
            'upc_code',
            'title',
            'expiration',
            'score',
            'user',
            'tags'
        )

class RecipeSerializer(serializers.ModelSerializer):

    class Meta:
        model=User_Recipes
        fields = (
            'recipe_id',
            'title',
            'image',
            'recipe_url',
            'user'
        )