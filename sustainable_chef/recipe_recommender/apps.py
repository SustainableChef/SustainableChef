from django.apps import AppConfig


class RecipeRecommenderConfig(AppConfig):
    name = 'recipe_recommender'
