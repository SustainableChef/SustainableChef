const userId = document.getElementById("userId").value;
const csrf_token = jQuery("[name=csrfmiddlewaretoken]").val();

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

const deleteSavedRecipe = (recipeId) => {
    let recipeNode = document.getElementById(`saved-${recipeId}`);
    let ulRecipeNode = document.getElementById("mySavedRecipes");
    let srData = `user_id=${userId}&recipe_id=${recipeId}`;
    $.ajax({
        type: "DELETE",
        url: '/myrecipes/',
        data: srData,
        success: function () {
            if (ulRecipeNode.textContent != "No recipes saved" && ulRecipeNode.childElementCount == 1) {
                listEl = document.createElement('li')
                listElText = document.createTextNode('No recipes saved')
                listEl.appendChild(listElText)
                ulRecipeNode.appendChild(listEl)
            }
            recipeNode.remove();
        }
    }).then(res => console.log(res));
}

const saveRecRecipe = (recipeId, title, image, recipeUrl) => {
    let firstRecipeNode = document.getElementById("mySavedRecipes").firstElementChild;
    let data = {
        title: title,
        image: image,
        recipe_id: recipeId,
        recipe_url: recipeUrl,
        user: userId
    }
    console.log(data)
    $.ajax({
        async: true,
        url: '/myrecipes/',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: "json",
        credentials: "include",
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        },
        success: function(){
            if (firstRecipeNode.textContent == "No recipes saved") {
                firstRecipeNode.remove();
            }
            getSavedRecipes()
        }
            }).then(res => console.log(res));
}

function getSavedRecipes() {
    fetch("/myrecipes").then(function(response) {
        return response.json();
    }).then(function(json){
        savedRecipes = json;
        addSavedRecipeChild(savedRecipes[savedRecipes.length -1]["title"], savedRecipes[savedRecipes.length -1]['id'], savedRecipes[savedRecipes.length -1]['recipe_url'])
    }).catch(function(err) {
        console.log('Fetch problem: ' + err.message)
    })
}

/*<li id='saved-{{ el.id }}'>
    <a class="boldFont" href={{ el.recipe_url }} target="blank">
        {{ el.title }}
    </a>
    <button onclick='deleteSavedRecipe({{ el.id }})'> X </button>
</li>
*/
//what the below function is modeled off of
function addSavedRecipeChild(title, id, recipeUrl) {
    let savedRecipesUl = document.getElementById("mySavedRecipes");
    let listEl = document.createElement("LI");
    listEl.id = 'saved-' + id
    let linkTag = document.createElement("a");
    let titleText = document.createTextNode(title);
    linkTag.appendChild(titleText)
    listEl.appendChild(linkTag)
    listEl.innerHTML = `<a class="boldFont" href="${recipeUrl}" target="blank">
                            ${title}
                        </a>
                        <button onclick='deleteSavedRecipe(${id})'> X </button>`
    savedRecipesUl.appendChild(listEl)
}

const deletePantryItem = (pantryId) => {
    let pantryNode = document.getElementById(`pantry-${pantryId}`);
    let ulPantryNode = document.getElementById("myPantryItems");
    let pantryData = `user_id=${userId}&food_id=${pantryId}`;
    $.ajax({
        type: "DELETE",
        url: '/ingredients/',
        data: pantryData,
        success: function () {
            if (ulPantryNode.textContent != "No food in pantry" && ulPantryNode.childElementCount == 1) {
                listEl = document.createElement('li')
                listElText = document.createTextNode('No food in pantry')
                listEl.appendChild(listElText)
                ulPantryNode.appendChild(listEl)
            }
            pantryNode.remove();
        }
    }).then(res => console.log(res));
}
