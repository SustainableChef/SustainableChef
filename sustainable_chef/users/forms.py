# users/forms.py
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordResetForm, SetPasswordForm
from .models import CustomUser, Profile

class CustomPasswordChange(PasswordResetForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['email'].widget = forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Email:'})
        self.fields['email'].label = False

class CustomPasswordResetConfirm(SetPasswordForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].widget = forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'})
        self.fields['new_password1'].label = False
        self.fields['new_password2'].widget = forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Confirm Password'})
        self.fields['new_password2'].label = False

class InitialSignupForm(forms.ModelForm):
    username = forms.CharField(label=False, max_length=25, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    email = forms.EmailField(label=False, widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'}))

    class Meta:
        model = CustomUser
        fields = ['username', 'email']


class CustomUserCreationForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'})
        self.fields['username'].label = False
        self.fields['username'].help_text = None
        self.fields['email'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email'})
        self.fields['email'].label = False
        self.fields['email'].help_text = None
        self.fields['email'].required = True
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Password'}) 
        self.fields['password1'].label = False
        self.fields['password1'].help_text = None
        self.fields['password2'].label = False
        self.fields['password2'].help_text = None
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control', 'placeholder':'Confirm Password'}) 

        #trys to find already inputted password, if none, it lets it go
        try:
            if kwargs['initial']['1-username']:
                user = kwargs['initial']['1-username']
                self.fields['username'].initial = user
        except KeyError:
            pass
        
        try:
            if kwargs['initial']['1-email']:
                email = kwargs['initial']['1-email']
                self.fields['email'].initial = email
        except KeyError:
            pass
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email')

class UserInformationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'First Name'})
        self.fields['first_name'].label = False
        self.fields['first_name'].help_text = None
        self.fields['last_name'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Last Name'})
        self.fields['last_name'].label = False
        self.fields['last_name'].help_text = None
        self.fields['other'].widget = forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Other'})
        self.fields['other'].label = False
        self.fields['other'].help_text = None
        
    class Meta:
        model = Profile
        fields = ['first_name', 'last_name', 'dairy', 'eggs', 'tree_nuts', 'peanuts', 'shellfish', 'wheat', 'soy', 'fish', 'other']

class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = CustomUser
        fields = ('username', 'email')