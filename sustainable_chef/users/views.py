from django.shortcuts import render
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password
from formtools.wizard.views import SessionWizardView
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from .forms import InitialSignupForm, CustomUserCreationForm, UserInformationForm, CustomPasswordChange, CustomPasswordResetConfirm
from .models import CustomUser, Profile
from django.contrib.auth import views

FORMS_REG = [('0', InitialSignupForm),
            ('1', CustomUserCreationForm),
            ('2', UserInformationForm)]

TEMPLATES_REG = {'0': 'users/signup.html',
                '1': 'users/middle_signup.html',
                '2':'users/end_signup.html'}
class SignUpWizard(SessionWizardView):
    instance = None

    def get_template_names(self):
        return [TEMPLATES_REG[self.steps.current]]

    def get_form_instance(self, step):
        if self.instance is None:
            self.instance = CustomUser()
        return self.instance

    def get_form_initial(self, step):
        # get the data for 1 on 2
        if step == '1':
            prev_data = self.storage.get_step_data('0')
            user = prev_data.get('0-username','')
            email = prev_data.get('0-email', '')
        
            return self.initial_dict.get(step, {'1-username': user, '1-email': email})

        return self.initial_dict.get(step, {})

    def done(self, form_list, **kwargs):
        auth_instance = CustomUser()
        profile_instance = Profile()
        for el in form_list:
            for key, value in el.cleaned_data.items():
                #print(str(key) + ':' + str(value))
                if key == 'username':
                    setattr(auth_instance, key, value)
                elif key == 'password1' or key == 'password2':
                    key = 'password'
                    setattr(auth_instance, key, make_password(value))
                elif key == 'email':
                    setattr(auth_instance, key, value)
                else:
                    setattr(profile_instance, key, value)
        auth_instance.save()
        setattr(profile_instance, 'user', auth_instance)
        profile_instance.save()
        login(self.request, auth_instance)
        return HttpResponseRedirect('home/')

class LogoutView(views.LogoutView):
    next_page = 'login'

class CustomPasswordResetView(views.PasswordResetView):
    form_class = CustomPasswordChange

class CustomPasswordResetConfirmView(views.PasswordResetConfirmView):
    form_class = CustomPasswordResetConfirm