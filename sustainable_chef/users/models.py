# users/models.py
from django.db import models
from django.contrib.auth.models import AbstractUser, User

class CustomUser(AbstractUser):
    pass

class Profile(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50, blank=True)
    last_name = models.CharField(max_length=50, blank=True)
    dairy = models.BooleanField(blank=True)
    eggs = models.BooleanField(blank=True)
    tree_nuts = models.BooleanField(blank=True)
    peanuts = models.BooleanField(blank=True)
    shellfish = models.BooleanField(blank=True)
    wheat = models.BooleanField(blank=True)
    soy = models.BooleanField(blank=True)
    fish = models.BooleanField(blank=True)
    other = models.CharField(max_length=100, blank=True)
