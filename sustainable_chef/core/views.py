from django.shortcuts import render
#from django.contrib.auth.forms import UserCreationForm
from users.forms import InitialSignupForm, CustomUserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import views

class LogoutView(views.LogoutView):
    next_page = 'login'