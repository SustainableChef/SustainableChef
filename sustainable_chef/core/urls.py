"""sustainable_chef URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from django.views.generic.base import TemplateView
from core.forms import MyAuthForm
from . import views
from users.forms import InitialSignupForm, CustomUserCreationForm, UserInformationForm
from users.views import SignUpWizard
from recipe_recommender import views as r_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', SignUpWizard.as_view([InitialSignupForm, CustomUserCreationForm, UserInformationForm]), name='signup'), #signup
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html', authentication_form = MyAuthForm), name='login'),
    path('logged_out', views.LogoutView.as_view(template_name='users/logged_out.html'), name='logged_out'),
    path('home/', r_views.home, name='home'),

    #might want to think about moving these over to 'recipe_recommender/urls.py but for now they stay
    path('recipes/', r_views.recipebyingredients, name='recipebyingredients'),
    path('recipe/<int:recipe_id>', r_views.recipe, name='recipe'),
    path('scanner/', r_views.scanner, name='scanner' ),
    path('expiring/', r_views.expiring_items, name='expiring_items'),

    path('myrecipes/', r_views.save_recipes, name='save_recipes'),

    path('ingredients/', r_views.ingredient_list),
    
    path('users/', include('users.urls')),
    
]
